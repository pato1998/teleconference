const express = require("express");
const http = require("http");
const socket = require("socket.io");
const path = require('path');
const passport = require('passport');
const flash = require('connect-flash')
const session = require('express-session')({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
});
const cookieParser = require('cookie-parser');
const sharedsession = require("express-socket.io-session");

const app = express();
const server = http.createServer(app);
const io = socket(server);

require('dotenv').config();
require('./database');
require('./config/passport');

//Views
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: false }));

//Middlewares
app.use(session)
app.use(flash())

//Passport
app.use(passport.initialize());
app.use(passport.session());

//Globals
app.use((req, res, next) => {
    res.locals.user = req.user || null
    next()
})

//Routes
app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/rooms', require('./routes/rooms'));

//Sockets
const users = {};
const socketToRoom = {};

io.use(sharedsession(session));
io.use((socket, accept) => {
    //session(socket.handshake, {}, function (err) {
    var session = socket.handshake.session

    if (!session.hasOwnProperty('passport')) {
        console.log('----- User has no active session, error');
        accept(new Error('Not authenticated'));
        return
    }

    const userData = session.passport.user || {};


    // is connected and good
    if (!userData) {
        console.log('----- User has no active session, error');
        accept(new Error('Not authenticated'));
    } else {
        console.log('----- Socket.io connection attempt successful');
        accept(null, session.userid !== null);
    }
    //});
})
require('./controllers/socket')(io, users, socketToRoom)

const port = process.env.PORT || 8000;
server.listen(port, () => console.log(`server is running on port ${port}`));


