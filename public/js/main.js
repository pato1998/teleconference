$(document).ready(function () {
    const route = window.location.pathname

    if (route.includes('dashboard')) {
        $('body').addClass('dashboard')
    } else {
        $('body').removeClass('dashboard')
    }
})