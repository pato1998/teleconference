
const templatesKey = {
    1: 'templateOne',
    2: 'templateTwo',
    3: 'templateThree',
    4: 'templateFour',
    'default': 'templateDefault'
}

const linkIcons = {
    'link': 'fa-external-link-alt',
    'unlink': 'fa-unlink'
}

let linkAssets = {
    'link': {
        circle: 'circle-red',
        title: 'Vincular',
        icon: linkIcons.link,
        btn: 'btnLink'
    },
    'unlink': {
        circle: 'circle-green',
        title: 'Desvincular',
        icon: linkIcons.unlink,
        btn: 'btnUnlink'
    }
}


const templates = {
    templateOne: (peersRef) => {
        let html = getVideoTemplateBase(12, null, peersRef[0])
        return html
    },

    templateTwo: (peersRef) => {
        let html = []
        let endHtml = ''
        let template

        peersRef.forEach((peer, index) => {
            template = getVideoTemplateBase(6, null, peer)
            html.push(template)
        });

        html.forEach(html => {
            endHtml += html
        })

        return endHtml
    },

    templateThree: (peersRef) => {
        let html = []
        let endHtml = ''

        peersRef.forEach((peer, index) => {
            let template
            if (index == 0) {
                template = getVideoTemplateBase(8, null, peer)
                html.push(template)

                html.push('<div class="col-4 sp">')
            } else {
                template = getVideoTemplateBase(12, ['hvh50'], peer)
                html.push(template)
            }

            if (index == 2) {
                html.push('</div>')
            }
        });

        html.forEach(html => {
            endHtml += html
        })

        return endHtml

    },

    templateFour: (peersRef) => {
        let html = ''

        peersRef.forEach(peer => {
            html += getVideoTemplateBase(6, ['hvh50'], peer)
        })

        return html
    },

    templateDefault: () => {

    }
}

const getVideoTemplateBase = (colNumber, classes, peer) => {
    //Create video
    const { user } = peer

    if (peer.link) {
        linkAsset = linkAssets.unlink
    } else {
        linkAsset = linkAssets.link
    }

    return `<div class="col-${colNumber} video sp ${classes}" id="video-${peer.peerID}" data-state=false data-col="${colNumber}">
                <div class="actions ${peer.isVideoReady ? 'd-flex' : 'd-none'} align-items-center">
                    <button class="btn btn-light ${linkAsset.btn}" data-socket-id="${peer.peerID}" title="${linkAsset.title}"><i class="fas ${linkAsset.icon}"></i></button>
                    <button class="btn btn-light ml-2 btnFullScreen" data-socket-id="${peer.peerID}" title="Pantalla completa"><i class="fas fa-expand"></i></button>
                    <h5 class="m-0 ml-2">${user.name + ' ' + user.lastname}</h5>
                    <div class="circle ${linkAsset.circle} ml-2"></div>
                </div>
            </div>`;
}

const getTemplateNotifications = (notification) => {
    const time = new Date(notification.time)

    return `<div>
                <p>
                    <b>${notification.user.name}</b> ${notification.description} <i class="fas fa-pause-circle"></i><br>
                    <span class="hour">${time.getHours() + ':' + time.getMinutes() + ' ' + (time.getHours() >= 12 ? 'PM' : 'AM')}</span>
                </p>
            </div>`
}

//used in host.js
const render = (peersRef) => {
    let total = peersRef.length;

    $('#videoPoint').empty()

    if (total == 0) return
    if (total > 4) total = 'default'

    const templateName = templatesKey[total]
    const templateHtml = templates[templateName](peersRef)

    $('#videoPoint').append(templateHtml)

    peersRef.forEach(peer => {
        addVideo(peer)
    })
}

const renderNotifications = ({ notifications }) => {
    const container = $('#messageContainer .messages')
    let html = ''

    notifications.forEach(notification => {
        html += getTemplateNotifications(notification)
    })

    $(container).empty()
    $(container).append(html)
}

//Used in point.js
const renderHost = (peer) => {
    if (peer.length)
        addVideo(peer[0])
}
