const videoGrid = document.getElementById('videoGrid')
const myVideo = document.createElement('video')
let socket = io('/')

//Video config
const videoConstraints = {
    height: 500,
    width: 500
};

let myStream
let hostVideo

let Peers = new Peer()
Peers.addSuscriptor(renderHost)

$(document).ready(function () {
    //Iniciar conexiones
    init()

    //Eventos socket
    socket.on('auth error', (data) => {
        console.log('auth error', data);
    })

    //Eventos DOM
    $(document).on('keypress', function (e) {
        if (e.which == 13) {
            const notify = { description: 'Persona en espera', user: user, time: new Date() }
            const peer = Peers.getPeers()
            if (peer) {
                const socketId = peer[0].user.socketId
                sendNotify({ socketId: socketId, notify: notify })
            }
        }
    });
})

const init = () => {
    navigator.mediaDevices.getUserMedia({ video: videoConstraints, audio: true }).then(stream => {
        myStream = stream
        myStream.getAudioTracks().forEach(track => { track.enabled = false })

        socket.emit('join', { roomId: roomId, userId: user._id });

        socket.on('users', users => {
            console.log('users')
            users.forEach(user => {
                const peer = createPeer(user.socketId, socket.id, myStream);
                Peers.addPeer({
                    peerID: user.socketId,
                    peer,
                    user
                })
            })
        })

        socket.on("user joined", data => {
            console.log('user joined')
            const peer = addPeer(data.signal, data.callerID, myStream);
            const { user } = data;

            Peers.updatePeer({
                peerID: data.callerID,
                peer,
                user
            })
        });

        socket.on("receiving returned signal", data => {
            console.log('receiving returned signal')
            const item = Peers.getPeers().find(p => p.peerID === data.id);
            item.peer.signal(data.signal);
        });

        socket.on('onLink', data => {
            console.log('link')
            myStream.getAudioTracks().forEach(track => track.enabled = true)
            hostVideo.muted = false
            $('#myVideoPoint').css('display', 'flex')
            $('#publicity').css('display', 'none')
        })
        socket.on('onUnlink', data => {
            console.log('unlink')
            myStream.getAudioTracks().forEach(track => track.enabled = false)
            hostVideo.muted = true
            $('#myVideoPoint').css('display', 'none')
            $('#publicity').css('display', 'flex')
        })

        socket.on('user-disconnect', data => {
            console.log('disconnect', data)
            const { socketId } = data
            removePeer(socketId)
        })
    });
}

const createPeer = (userToSignal, callerID, stream) => {
    const peer = new SimplePeer({
        initiator: true,
        trickle: false,
        stream,
        config: {
            iceServers: [
                {
                    urls: "stun:stun.stunprotocol.org"
                },
                {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
            ]
        }

    });

    peer.on("signal", signal => {
        socket.emit("sending signal", { userToSignal, callerID, signal })
    })

    return peer;
}

function addPeer(incomingSignal, callerID, stream) {
    const peer = new SimplePeer({
        initiator: false,
        trickle: false,
        stream,
        config: {
            iceServers: [
                {
                    urls: "stun:stun.stunprotocol.org"
                },
                {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
            ]
        }
    })

    peer.on("signal", signal => {
        socket.emit("returning signal", { signal, callerID })
    })

    peer.signal(incomingSignal);

    return peer;
}

const addVideo = (peer) => {
    const { user, peerID } = peer

    if (hostVideo) {
        $(`#myVideoPoint`).append(hostVideo)
        return
    }

    //if (!user) return
    peer.peer.on('stream', stream => {
        //if (peersOnStream.includes(peer.user.socketId)) return;

        let video = document.createElement('video')

        video.muted = true
        video.srcObject = stream
        video.addEventListener('loadedmetadata', () => {
            video.play()
        })

        hostVideo = video

        $('#myVideoPoint').append(hostVideo)

        myStream.getAudioTracks().forEach(track => { track.enabled = false })
        socket.emit('video-ready', { socketId: socket.id, roomId: roomId })
    })
}

const removePeer = (socketId) => {
    Peers.removePeer(socketId)
    hostVideo = null
    $(`#myVideoPoint`).empty().css('display', 'none')
    $('#publicity').css('display', 'flex')
}

const sendNotify = ({ socketId, notify }) => {
    socket.emit('notify', { socketId: socketId, notify: notify })
}
