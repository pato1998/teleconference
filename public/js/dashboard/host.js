
const videoGrid = document.getElementById('videoGrid')
const myVideo = document.createElement('video')
let socket = io('/')

//Config
const videoConstraints = {
    height: 500,
    width: 500
};

let videos = {}
let myStream
let peerLink = null

//Peers 
let Peers = new Peer()
Peers.addSuscriptor(render)

//Notifications
let Notifications = new Notification()
Notifications.addSuscriptor(renderNotifications)

$(document).ready(function () {
    //Iniciar conexiones
    init()

    //Eventos socket
    socket.on('connect_error', (data) => {
        window.location.href = '/login'
    })

    socket.on('notify', (data) => {
        Notifications.add(data)
    })

    //Eventos DOM
    $(document).on('click', '.btnLink', (e) => {
        const peerLink = Peers.getLinkPeer()

        if (peerLink) {
            alert('Ya se encuentra vinculado a ' + peerLink.user.name)
            return
        }

        const socketId = e.currentTarget.dataset.socketId
        const btn = $(e.currentTarget)
        const i = btn.find('i')
        const container = $(`#video-${socketId}`);

        myStream.getAudioTracks().forEach(track => track.enabled = true)
        btn.removeClass('btn-primary btnLink').addClass('btn-danger btnUnlink')
        i.removeClass(linkIcons.link).addClass(linkIcons.unlink)
        i.attr('title', 'Desvincular')
        container.find('.circle').removeClass('circle-red').addClass('circle-green')

        if (Peers.linkPeer(socketId)) {
            alert('Ya se encuentra vinculado')
        }
        link(socketId)
    })

    $(document).on('click', '.btnUnlink', (e) => {
        const socketId = e.currentTarget.dataset.socketId
        const btn = $(e.currentTarget)
        const i = btn.find('i')
        const container = $(`#video-${socketId}`);

        myStream.getAudioTracks().forEach(track => track.enabled = false)
        btn.removeClass('btn-danger btnUnlink').addClass('btn-primary btnLink')
        i.removeClass(linkIcons.unlink).addClass(linkIcons.link)
        i.attr('title', 'Vincular')
        container.find('.circle').removeClass('circle-green').addClass('circle-red')

        Peers.unlinkPeer()
        unlink(socketId)
    })

    $(document).on('click', '.btnFullScreen', (e) => {

        if (Peers.getPeers().length < 2) {
            return
        }

        const socketId = e.currentTarget.dataset.socketId
        const video = $(`#video-${socketId}`)
        const state = video.data('state')

        if (!state) {
            $('.video').each(function () {
                if ($(this).attr('id') != `video-${socketId}`) {
                    $(this).css('display', 'none')
                }
            })

            video.data('state', true)
            video.removeClass(['col-6', 'hvh50']).addClass(['col-10', 'fullScreen'])
        } else {
            $('.video').each(function () {
                if ($(this).attr('id') != `video-${socketId}`) {
                    $(this).css('display', 'block')
                }
            })

            const col = video.data('col')
            let addClass = [`col-${col}`]
            if (col == 12)
                addClass.push('hvh50')


            video.data('state', false)
            video.removeClass(['col-10', 'fullScreen']).addClass(addClass)
        }
    })
})

const init = () => {
    navigator.mediaDevices.getUserMedia({ video: videoConstraints, audio: true }).then(stream => {
        myStream = stream
        myStream.getAudioTracks().forEach(track => { track.enabled = false })
        if (user.type == 'host') {
            addMyVideo(myStream)
        }

        socket.emit('join', { roomId: roomId, userId: user._id });

        socket.on('users', users => {
            console.log('users')
            const peers = []
            users.forEach(user => {
                const peer = createPeer(user.socketId, socket.id, myStream);
                peers.push({
                    peerID: user.socketId,
                    peer,
                    user
                })

                Notifications.add({ description: 'Conectado', user: user, time: new Date() })
            })

            Peers.addPeers(peers)
        })

        socket.on("user joined", data => {
            console.log('user joined')
            const peer = addPeer(data.signal, data.callerID, myStream);
            const { user } = data;

            Peers.updatePeer({
                peerID: data.callerID,
                peer,
                user
            })
        });

        socket.on("receiving returned signal", data => {
            console.log('receiving returned signal')
            const item = Peers.getPeers().find(p => p.peerID === data.id);
            item.peer.signal(data.signal);
        });

        socket.on('user-disconnect', data => {
            console.log('disconnect')
            const { socketId, user } = data
            removePeer(socketId)
            Notifications.add({ description: 'Desconectado', user: user, time: new Date() })
        })

        socket.on('video-ready', data => {
            const { socketId } = data
            Peers.setVideoReady(socketId)
            $(`#video-${socketId} .actions`).removeClass('d-none').addClass('d-flex')

        })
    });
}

const createPeer = (userToSignal, callerID, stream) => {
    const peer = new SimplePeer({
        initiator: true,
        trickle: false,
        stream,
        config: {
            iceServers: [
                {
                    urls: "stun:stun.stunprotocol.org"
                },
                {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
            ]
        }

    });

    peer.on("signal", signal => {
        socket.emit("sending signal", { userToSignal, callerID, signal })
        console.log('sending signal')
    })

    return peer;
}

function addPeer(incomingSignal, callerID, stream) {
    const peer = new SimplePeer({
        initiator: false,
        trickle: false,
        stream,
        config: {
            iceServers: [
                {
                    urls: "stun:stun.stunprotocol.org"
                },
                {
                    urls: 'turn:numb.viagenie.ca',
                    credential: 'muazkh',
                    username: 'webrtc@live.com'
                },
            ]
        }
    })

    peer.on("signal", signal => {
        socket.emit("returning signal", { signal, callerID })
    })

    peer.signal(incomingSignal);

    return peer;
}

const addMyVideo = (stream) => {
    if (user.type != 'host')
        return;

    myVideo.srcObject = stream
    myVideo.muted = true
    myVideo.addEventListener('loadedmetadata', () => {
        myVideo.play()
    })

    $('#myVideo').append(myVideo)
}

const addVideo = (peer) => {
    const { user, peerID } = peer

    if (Peers.getVideo(peerID)) {
        $(`#video-${peerID}`).append(Peers.getVideo(peerID))
        return
    }

    peer.peer.on('stream', stream => {
        //if (peersOnStream.includes(peer.user.socketId)) return;
        console.log('in stream')
        let video = document.createElement('video')

        video.srcObject = stream
        video.addEventListener('loadedmetadata', () => {
            video.play()
        })

        $(`#video-${peerID}`).append(video)

        Peers.addVideo(peerID, video)

        //myStream.getAudioTracks().forEach(track => { track.enabled = false })
    })
}

const removePeer = (socketId) => {
    Peers.removePeer(socketId)
}

const link = (socketId) => {
    socket.emit('link', { socketId: socketId })
    //Cambiar btn, verificar desconexion con los demas, etc
}

const unlink = (socketId) => {
    socket.emit('unlink', { socketId: socketId })
    //Cambiar btn, verificar desconexion con los demas, etc
}
