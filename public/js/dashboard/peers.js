class Peer {
    constructor() {
        this.peers = []
        this.videos = []
        this.suscriptors = []
    }

    addPeer(peer) {
        this.peers.push(peer)
        this.notify()
    }

    addPeers(peers) {
        this.peers.push(...peers)
        this.notify()
    }

    removePeer(peerId) {
        const peer = this.peers.filter(peer => peer.peerID == peerId)
        console.log({ peer })
        peer[0].peer.destroy()

        const peers = this.peers.filter(peer => peerId !== peer.peerID)
        this.peers = peers
        this.notify()
    }

    updatePeer(peer) {
        const peers = this.peers.filter(p => p.peerID !== peer.peerID)
        this.peers = peers
        this.peers.push(peer)
        this.notify()
    }

    addVideo(id, video) {
        const peer = this.peers.filter(peer => peer.peerID == id)
        peer[0].video = video

        const peers = this.peers.filter(peer => peer.peerID != id)
        this.peers = peers

        this.peers.push(peer[0])
    }

    removeVideo(id) {
        const peer = this.peers.filter(peer => peer.peerID == id)
        delete peer[0].video

        const peers = this.peers.filter(peer => peer.peerID != id)
        this.peers = peers

        this.peers.push(peer[0])
    }

    getVideo(id) {
        const peer = this.peers.filter(peer => peer.peerID == id)
        return peer[0].video ? peer[0].video : null
    }

    addSuscriptor(callback) {
        this.suscriptors.push(callback)
    }

    notify() {
        this.suscriptors.forEach((suscriptor) => {
            suscriptor(this.peers)
        })
    }

    getPeers() {
        return this.peers
    }

    getPeer(id) {
        const peer = this.peers.filter(peer => peer.peerID == id)
        return peer[0]
    }

    getUser(id) {
        const peer = this.peers.filter(peer => peer.peerID == id)
        return peer[0].user
    }

    linkPeer(id) {
        const existLink = this.getLinkPeer()
        console.log({ existLink })
        if (existLink) {
            return existLink
        }

        const peer = this.peers.filter(peer => peer.peerID == id)
        peer[0].link = true

        this.update(peer[0])

        console.log({ peer })
    }

    unlinkPeer() {
        const peer = this.peers.filter(peer => peer.link == true)
        if (peer) {
            peer[0].link = false;
            this.update(peer[0])
        }
    }

    getLinkPeer() {
        const peerLink = this.peers.filter(peer => peer.link == true)
        return peerLink[0] ? peerLink[0] : null;
    }

    update(p) {
        const peers = this.peers.filter(peer => peer.peerID != p.peerID)
        this.peers = peers

        this.peers.push(p)
    }

    setVideoReady(id) {
        const peer = this.getPeer(id)
        peer.isVideoReady = true

        this.update(peer)
    }
}
