class Notification extends Suscriptor {
    constructor() {
        super()
        this.data.notifications = []
    }

    add(notification) {
        this.data.notifications.push(notification)
        this.notify()
    }

    get() {
        return this.data.notifications
    }
}