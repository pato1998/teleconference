class Suscriptor {
    constructor() {
        this.data = {}
        this.suscriptors = []
    }

    addSuscriptor(callback) {
        this.suscriptors.push(callback)
    }

    notify() {
        this.suscriptors.forEach((suscriptor) => {
            suscriptor(this.data)
        })
    }
}