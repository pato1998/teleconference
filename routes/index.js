const express = require('express');
const router = express.Router();
const userController = require('../controllers/users');
const dashboardController = require('../controllers/dashboard');
const { isAuth } = require('../helpers/auth');

router.get('/', userController.login);
router.get('/register', userController.register);
router.post('/register', userController.postRegister);
router.get('/login', userController.login);
router.post('/login', userController.postLogin);
router.get('/dashboard', isAuth, dashboardController.index);

router.get('/test', (req, res) => {
    res.render('test')
});


module.exports = router;