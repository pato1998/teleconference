const express = require('express');
const router = express.Router();
const roomsController = require('../controllers/rooms');

router.get('/rooms', roomsController.postRoom);


module.exports = router;    