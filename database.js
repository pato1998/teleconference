const mongoose = require('mongoose')

//const url = 'mongodb://localhost/teleconference'
const url = `mongodb+srv://${process.env.BD_USER}:${process.env.BD_PASSWORD}@cluster0.9odbh.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`
mongoose.connect(url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(bd => console.log('db connect'))
    .catch(err => console.log('error', err));