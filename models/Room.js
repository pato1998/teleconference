const mongoose = require('mongoose');
const { Schema, ObjectId } = mongoose;

const RoomSchema = new Schema({
    name: { type: String, required: true },
    users: [{ type: ObjectId, ref: 'User' }],
    date: { type: Date, default: Date.now }
}, {
    timestamps: true
});

module.exports = mongoose.model('Room', RoomSchema)