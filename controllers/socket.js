const User = require('../models/User');

module.exports = (io, users, socketToRoom) => {
    io.on('connection', socket => {

        const { password, __v, ...user } = socket.handshake.session.passport.user

        socket.on('join', data => {
            console.log('Join!')
            const roomId = data.roomId;
            const userId = data.userId;

            if (!user || user._id !== userId) {
                socket.emit('auth error', { msg: 'Usuario no autenticado' });
                return;
            }

            if (users[roomId]) {
                const length = users[roomId].length;
                if (length === 4) {
                    socket.emit("room full");
                    return;
                }
                users[roomId].push({ socketId: socket.id, ...user }); //Remove data like password, etc
            } else {
                users[roomId] = [{ socketId: socket.id, ...user }]; //Remove data like password, etc
            }

            socketToRoom[socket.id] = roomId;

            if (user.type == 'host') {
                //Devuelve todos los users menos el suyo
                const usersInThisRoom = users[roomId].filter(user => user.type !== 'host');
                const host = users[roomId].filter(user => user.type == 'host');

                socket.emit('users', usersInThisRoom);
                //socket.broadcast.emit('users', host);
            } else {
                const host = users[roomId].filter(user => user.type == 'host');
                const newUser = users[roomId].filter(user => user.socketId == socket.id)
                if (host.length) {
                    socket.emit('users', host)
                    //socket.to(host[0].socketId).emit('users', newUser)
                }
            }
        })

        socket.on("sending signal", data => {
            console.log('sending signal')
            const user = socket.handshake.session.passport.user;
            io.to(data.userToSignal).emit('user joined', { signal: data.signal, callerID: data.callerID, user: user });
        });

        socket.on("returning signal", data => {
            console.log('returning signal')
            io.to(data.callerID).emit('receiving returned signal', { signal: data.signal, id: socket.id });
        });

        socket.on('disconnect', async (data) => {
            const roomID = socketToRoom[socket.id];
            let room = users[roomID];
            if (room) {
                room = room.filter(user => user.socketId !== socket.id);
                users[roomID] = room;
                delete socketToRoom[socket.id];

                const _User = await User.findOne({ email: user.email })
                _User.isLive = false
                await _User.save()

                console.log(_User, 'desconectado')
            }

            socket.broadcast.emit('user-disconnect', { socketId: socket.id, user: user })
        });

        socket.on('link', data => {
            const { socketId } = data
            socket.to(socketId).emit('onLink')
        })

        socket.on('unlink', data => {
            console.log('unlink')
            const { socketId } = data
            socket.to(socketId).emit('onUnlink')
        })

        socket.on('notify', data => {
            const { socketId, notify } = data
            socket.to(socketId).emit('notify', notify)
        })

        socket.on('video-ready', data => {
            const { socketId, roomId } = data
            const host = users[roomId].filter(user => user.type == 'host');

            socket.to(host[0].socketId).emit('video-ready', { socketId: socketId })
        })
    })
}   
