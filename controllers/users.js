const passport = require('passport');
const User = require('../models/User');

const users = {
    register: (req, res) => {
        res.render('register');
    },

    login: (req, res) => {
        res.render('login', { error: req.flash('error') });
    },

    postLogin: passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true
    }),

    register: (req, res) => {
        res.render('register');
    },

    postRegister: async (req, res) => {
        const { name, lastname, email, password, type } = req.body;

        const user = new User({ name, lastname, email, password, type })
        user.password = await user.encryptPassword(password)
        await user.save();

        console.log('Usuario registrado', user);
    },

    renderLayout: (req, res) => {
        res.render('partials/users/layout')
    },

    resetLive: async (req, res) => {
        await User.updateMany({ 'isLive': true }, { '$set': { 'isLive': false } })

        res.redirect('/login')
    }
}

module.exports = users;