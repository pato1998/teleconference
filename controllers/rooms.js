const passport = require('passport');
const Room = require('../models/Room')

const rooms = {
    room: (req, res) => {
        res.render('rooms/form')
    },
    postRoom: async (req, res) => {
        const name = 'Room 1';
        const users = ['60936695cce92d3d3e17259b', '60937575b2cf21539ab35906', '609375a5b2cf21539ab35907'];

        const room = new Room();
        room.name = name;
        users.forEach(user => {
            room.users.push(user)
        });

        await room.save()

        console.log('Rooom created!');
    }
}

module.exports = rooms;