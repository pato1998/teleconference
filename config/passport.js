const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');

passport.use(new LocalStrategy({
    usernameField: "email",
}, async (email, password, done) => {
    const user = await User.findOne({ email: email })

    if (!user) {
        return done(null, false, { message: 'Credenciales invalidas' }) //usuario, error, msg
    } else {
        /* user.isLive = false
         await user.save()*/

        if (user.isLive) {
            console.log('is live')
            return done(null, false, { message: 'El usuario ya ha iniciado sesion' })
        } else {
            const match = await user.matchPassword(password)
            user.isLive = true
            await user.save()

            if (match) {
                return done(null, user);
            } else {
                return done(null, false, { message: 'Credenciales invalidas' })
            }
        }
    }
}));

passport.serializeUser((user, done) => {
    done(null, user);
})

passport.deserializeUser((id, donde) => {
    User.findById(id, (error, user) => {
        donde(error, user);
    })
});

exports.module = passport;